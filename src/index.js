import {Observable} from 'rxjs';

const result = document.getElementById('result');
const button = document.getElementById('button');
const click = Observable
  .fromEvent(button, 'click');

const loadWithXhr = url => (
  Observable.create(observer => {
    const xhr = new XMLHttpRequest();

    const onLoad = () => {
      if (xhr.status === 200){
        let data = JSON.parse(xhr.responseText);
        observer.next(data);
        observer.complete();
      } else {
        observer.error(xhr.statusText);
      }
    };

    xhr.addEventListener('load', onLoad);

    xhr.open('GET', url);
    xhr.send();

    return () => {
      console.log('xhr cleanup')
      xhr.removeEventListener('load', onLoad);
      xhr.abort();
    }
  })
);

const loadWithFetch = url => (
  // defer is to lazy execute what's inside fromPromise
  Observable.defer(() => (
    Observable.fromPromise(
      fetch(url)
        .then(response => {
          if (response.status === 200){
            return response;
          } else {
            let error = new Error(response.statusText);
            error.response = response;
            return Promise.reject(error);
          }
        })
        .then(response => response.json())
    )
  ))
);

const loadMovies = movies => {
  movies.forEach(m => {
    var movieElement = document.createElement('div');
    movieElement.innerText = m.title;
    result.appendChild(movieElement);
  });
};

const retryStrategy = ({attempts, delay} = {attempts: 5, delay: 500}) => (
  errors => (
    errors
      .scan((a, e) => ({count: a.count + 1, error: e}), {count: 0})
      .flatMap(v => (v.count <= attempts ? Observable.of(v) : Observable.throw(v.error)))
      .delay(delay)
  )
);

const subscription =
loadWithXhr('/movies')
//loadWithXhr('/moviess')
//loadWithFetch('/moviess')
  .retryWhen(retryStrategy())
  .subscribe(
    loadMovies,
    e => console.log(`error: ${e}`),
    () => console.log('complete')
  );

//subscription.unsubscribe();

click
  .flatMap(e => loadWithFetch('/movies').retryWhen(retryStrategy({attempts: 3, delay: 1000})))
  //.flatMap(e => loadWithFetch('/moviess').retryWhen(retryStrategy()))
  .subscribe(
    loadMovies,
    e => console.log(`error: ${e}`),
    () => console.log('complete')
  );
